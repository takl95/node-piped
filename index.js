exports.getInput = () => new Promise((resolve, reject) => {
  const stdin = process.stdin
  let data = ""

  stdin.setEncoding("utf8")
  stdin.on("data", chunk => {
    data += chunk
  })

  stdin.on("end", () => {
    resolve(data.replace(/\n/g, ''))
  })

  stdin.on("error", reject)
})
